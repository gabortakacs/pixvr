﻿using UnityEngine;

namespace pixvr
{
	public static class Consts 
	{
		public const float MIN_MASS = 0.1f;
		public const float MAX_MASS = 1000.0f;

#if ALWAYS_SHOW_SURFACES
		public const float MIN_SURFACE_ALPHA = 0.05f;
#else
		public const float MIN_SURFACE_ALPHA = 0.0f;
#endif

#if USE_LIGHTSPEED_LIMIT
		public const float LIGHTSPEED = 200.0f;
		public const float SQR_LIGHTSPEED = 40000.0f;
#endif
	}
}
