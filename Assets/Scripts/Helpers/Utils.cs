﻿using UnityEngine;

namespace pixvr
{
	public static class Utils
	{
		public static bool ValidateScale(Vector3 currentScale, Vector3 minScale, out Vector3 corrigatedScale)
		{
			corrigatedScale = currentScale;
			bool ret = true;
			ret &= ValidateScale(currentScale.x, minScale.x, ref corrigatedScale.x);
			ret &= ValidateScale(currentScale.y, minScale.y, ref corrigatedScale.y);
			ret &= ValidateScale(currentScale.z, minScale.z, ref corrigatedScale.z);
			return ret;
		}

		public static bool ValidateScale(float currentScale, float minScale, ref float corrigatedScale)
		{
			if (currentScale >= minScale)
				return true;
			corrigatedScale = minScale;
			return false;
		}
	}
}
