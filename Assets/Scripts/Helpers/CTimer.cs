﻿using System;

namespace pixvr
{
	public class CTimer
	{
		private float _duration;
		private Action _onStart;
		private Action<float> _onUpdate;
		private Action _onFinished;

		private float _timer;

		private bool _isRunning = false;

		public bool IsRunning
		{
			get => _isRunning;
			set
			{
				_isRunning = value;
				if (value)
				{
					Reset();
					_onStart?.Invoke();
				}
				else
					_onFinished?.Invoke();
			}
		}

		public CTimer(float duration, Action onStart, Action<float> onUpdate, Action onFinished)
		{
			_duration = duration;
			_onStart = onStart;
			_onUpdate = onUpdate;
			_onFinished = onFinished;
			_timer = 0.0f;
		}

		private void Reset()
		{
			_timer = 0.0f;
		}

		public void Update(float deltaTime)
		{
			if (!IsRunning)
				return;

			_timer += deltaTime;
			if (_timer < _duration)
				_onUpdate?.Invoke(_timer / _duration);
			else
				IsRunning = false;
		}
	}
}
