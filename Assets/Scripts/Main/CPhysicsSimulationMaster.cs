﻿using UnityEngine;
using pixvr.main.cube;
using pixvr.main.sphere;
using pixvr.main.physics;

namespace pixvr.main
{
	public class CPhysicsSimulationMaster : MonoBehaviour
	{
		[SerializeField] private Vector3 _cubeStartScale = Vector3.one;
		[SerializeField] private float _sphereStartScale = 1.0f;
		[SerializeField] private float _surfaceBlinkDurationOnCollision = 0.25f;
		[SerializeField] [Range(0.0f, 1.0f)] private float _energyAbsorbationOnCollision = 0.05f;

		[SerializeField] private CCube _cube;
		[SerializeField] private CSphere _sphere;

		private CPhysicsController _physicsController;

		public void Awake()
		{
			_cube.Init(_cubeStartScale, _sphereStartScale, _surfaceBlinkDurationOnCollision);
			_sphere.Init(_sphereStartScale, () => _energyAbsorbationOnCollision);
			_physicsController = new CPhysicsController(_cube, _sphere);
		}

		private void Update()
		{
			_physicsController.Update(Time.deltaTime);
			_cube.StoreStates();
		}

#if UNITY_EDITOR
		public void OnValidate()
		{
			if (_sphereStartScale < 0.1f)
				_sphereStartScale = 0.1f;
			if (!Utils.ValidateScale(_cubeStartScale, Vector3.one * Mathf.Max(0.1f, _sphereStartScale), out var corrigatedScale))
				_cubeStartScale = corrigatedScale;
		}
#endif
	}
}
