﻿using UnityEngine;

namespace pixvr.main.sphere
{
	public class CSphere : MonoBehaviour
	{
		[SerializeField] [Range(Consts.MIN_MASS, Consts.MAX_MASS)] private float _mass = Consts.MIN_MASS;

		[ReadOnly] [SerializeField] private Vector3 _velocity = Vector3.zero;

		public float Radius { get => transform.lossyScale.x / 2; }
		public Vector3 Velocity { get => _velocity; }

		private CTimer _absortTimer;
		private Delegate0<float> EnenyAbsorbationOnCollision { get; set; }

		public void Init(float scale, Delegate0<float> enenyAbsorbationOnCollision)
		{
			transform.localScale = Vector3.one * scale;
			EnenyAbsorbationOnCollision = enenyAbsorbationOnCollision;

			_absortTimer = new CTimer(0.05f,
				() => { }, (float rate) => { }, () => DoAbsorbEnergy()
				);
		}

		public void Update()
		{
			transform.position = transform.position + _velocity * Time.deltaTime;
			_absortTimer.Update(Time.deltaTime);
#if USE_LIGHTSPEED_LIMIT
			CorrigateSpeed();
#endif
		}

		public void AddForce(Vector3 force)
		{
			_velocity += force / (_mass * Time.deltaTime);
		}

		public void ReflectVelocity(Vector3 inNormal)
		{
			_velocity = Vector3.Reflect(_velocity, inNormal);
		}

		public void CeaseOverlap(Vector3 overlapVector)
		{
			transform.position += overlapVector;
		}

		public void AbsorbEnergy()
		{
			_absortTimer.IsRunning = true;
		}

		private void DoAbsorbEnergy()
		{
			_velocity *= 1.0f - EnenyAbsorbationOnCollision.Invoke();
		}

#if USE_LIGHTSPEED_LIMIT
		private void CorrigateSpeed()
		{
			if(_velocity.sqrMagnitude > Consts.SQR_LIGHTSPEED)
			{
				_velocity = _velocity.normalized * Consts.LIGHTSPEED;
			}	
		}
#endif
	}
}
