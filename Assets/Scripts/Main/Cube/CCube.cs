﻿using UnityEngine;
using System.Collections.Generic;

namespace pixvr.main.cube
{
	public class CCube : MonoBehaviour
	{
		[SerializeField] [Range(Consts.MIN_MASS, Consts.MAX_MASS)] private float _mass = Consts.MIN_MASS;
		[SerializeField] private List<CCubeFace> _faces;
		[SerializeField] private Gradient _faceColors;

		private Vector3 _minScale;

		public List<CCubeFace> Faces { get => _faces; }
		public float Mass { get => _mass; }

		public void Init(Vector3 scale, float minScale, float blinkDuration)
		{
			_minScale = Vector3.one * minScale;
			transform.localScale = scale;
			InitFaces(blinkDuration);
		}

		private void InitFaces(float blinkDuration)
		{
			for (int i = 0; i < _faces.Count; ++i)
				_faces[i].Init(_faceColors.Evaluate(i / (float)_faces.Count), blinkDuration);
		}

		public void StoreStates()
		{
			foreach (var curr in _faces)
				curr.StoreStates();
		}

		public void Update()
		{
			if (Utils.ValidateScale(transform.localScale, _minScale, out var corrigatedScale))
				return;
			transform.localScale = corrigatedScale;
		}
	}
}
