﻿using UnityEngine;
using System;

namespace pixvr.main.cube
{
	[Serializable]
	public class CCubeFace : CFlat
	{
		[SerializeField] private CCubeSurface _surface;

		private CTimer _blinkTimer = null;

		private float SurfaceAlpha { set => _surface.SetSurfacesAlpha(value); }

		public void Init(Color surfaceColor, float blinkDuration)
		{
			Init();
			_blinkTimer = new CTimer(blinkDuration,
					() => SurfaceAlpha = 1.0f,
					(float rate) => SurfaceAlpha = 1 - rate + Consts.MIN_SURFACE_ALPHA,
					() => SurfaceAlpha = Consts.MIN_SURFACE_ALPHA
					);
			_surface.Init(surfaceColor, Consts.MIN_SURFACE_ALPHA);
		}

		public void Update()
		{
			_blinkTimer?.Update(Time.deltaTime);
		}

		public void StartBlink()
		{
			_blinkTimer.IsRunning = true;
		}
	}
}
