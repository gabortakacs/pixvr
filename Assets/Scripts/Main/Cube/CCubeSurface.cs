﻿using UnityEngine;
using System.Collections.Generic;

namespace pixvr.main.cube
{
	public class CCubeSurface : MonoBehaviour
	{
		[SerializeField] private List<Renderer> _renderers;

		public void Init(Color color, float startAlpha)
		{
			foreach (var curr in _renderers)
			{
				curr.material.color = color;
				SetSurfacesAlpha(startAlpha);
			}
		}

		public void SetSurfacesAlpha(float alpha)
		{
			foreach (var curr in _renderers)
			{
				SetSurfacesAlpha(curr, alpha);
			}
		}

		private void SetSurfacesAlpha(Renderer renderer, float alpha)
		{
			var color = renderer.material.color;
			color.a = alpha;
			renderer.material.color = color;
		}
	}
}
