﻿using UnityEngine;

namespace pixvr.main.cube
{
    public class CFlat : MonoBehaviour
    {
        [SerializeField] private Transform _faceTransform;

        private Vector3 _prevPosition;
        private Vector3 _prevNormalVector;

        public Vector3 NormalVector
        {
            get => _faceTransform.up;
        }

        public void Init()
        {
            StoreTransformStates();
        }

        public float GetDistance(Vector3 point)
        {
            return Vector3.Dot(point - _faceTransform.position, NormalVector);
        }

        public Vector3 GetVelocity(Vector3 point)
        {
            return GetVelocityFromMovement() + GetVelocityFromRotation(point);
        }

        private Vector3 GetVelocityFromMovement()
        {
            return _faceTransform.position - _prevPosition;
        }

        private Vector3 GetVelocityFromRotation(Vector3 point)
        {
            return Vector3.Angle(_prevNormalVector, NormalVector) * NormalVector;
        }

        public void StoreStates()
        {
            StoreTransformStates();
        }

        private void StoreTransformStates()
        {
            _prevPosition = _faceTransform.position;
            _prevNormalVector = NormalVector;
        }
    }
}
