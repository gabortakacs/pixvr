﻿using UnityEngine;
using System.Collections.Generic;
using pixvr.main.cube;
using pixvr.main.sphere;

namespace pixvr.main.physics
{
	public class CPhysicsController
	{
		private CCube _cube;
		private CSphere _sphere;

		private List<CCubeFace> CubeFaces { get => _cube.Faces; }

		public CPhysicsController(CCube cube, CSphere sphere)
		{
			_cube = cube;
			_sphere = sphere;
		}

		public void Update(float deltaTime)
		{
			CheckCollision(deltaTime);
		}

		private void CheckCollision(float deltaTime)
		{
			var sphereRadius = _sphere.Radius;
			foreach (var currFace in CubeFaces)
			{
				if (!CheckCollision(sphereRadius, currFace, out var distance))
					continue;
				OnCollision(currFace, sphereRadius, distance, deltaTime);
			}
		}

		private void OnCollision(CCubeFace currFace, float sphereRadius, float distance, float deltaTime)
		{
			currFace.StartBlink();
			var forceFromCube = currFace.GetVelocity(_sphere.transform.position) * _cube.Mass * deltaTime;
			_sphere.CeaseOverlap(GetOverlapVector(sphereRadius, distance, currFace));
			if (Mathf.Abs(Vector3.Angle(currFace.NormalVector, _sphere.Velocity)) > 90)
				_sphere.ReflectVelocity(currFace.NormalVector);
			_sphere.AddForce(forceFromCube);
			_sphere.AbsorbEnergy();
		}

		private Vector3 GetOverlapVector(float sphereRadius, float distance, CCubeFace currFace)
		{
			return currFace.NormalVector * (sphereRadius - distance);
		}

		private bool CheckCollision(float sphereRadius, CCubeFace face, out float distance)
		{
			distance = face.GetDistance(_sphere.transform.position);
			return distance <= sphereRadius;
		}
	}
}
